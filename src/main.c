/*
 * Brian Chrzanowski
 * Sat Sep 08, 2018 01:15
 *
 * Brewing Grounds Pocket CHIP control program
 *
 * This will have the following features:
 *
 *		I2C Linux Interface controlling logic
 *		Data Logging
 *			Data gets logged to a simple CSV that can be defined in the config
 *				file
 *		Web Interface
 *			Served webpages
 *
 * NOTES
 *
 *		The i2c usage in this program is a little different than might be
 *		seen elsewhere. Mainly because I've setup a convention that we read
 *		some bytes from a device.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#include <inttypes.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <arpa/inet.h>
#include <microhttpd.h>

#include "libsoc_gpio.h"
#include "libsoc_debug.h"

#include "helper.h"
#include "io.h"
#include "web.h"
#include "brew/brewing-grounds.h"

#define OUTPUT_FILE "data.csv"
#define CONFIG_FILE "brew.cfg"
#define MASTER_UPS 0.05
#define UPDATES_PER_SEC (1000 / MASTER_UPS)

// you need to look on the pin listing (hopefully here) to find which pin
// numbers do what
#define GPIO_BOILER_SWITCH 135
#define GPIO_HOTPLATE_SWITCH 137

void coffee_doloop(int file, int *addr, grounds_t *data);
void coffee_control(grounds_t *data);
int check_change(grounds_t *curr, grounds_t *new);
gpio *gpio_setup(int pin_num, int direction);
int gpio_free(gpio *ptr);
int gpio_input_read(gpio *ptr);
void data_dump(long nonce, struct grounds_t *ptr);
double inttemp_to_doubtemp(uint16_t val);
double convert_c_to_f(double c);

int main(int argc, char **argv)
{
	long nonce;
	struct MHD_Daemon *web_daemon;
	gpio *boiler_sw, *hotplate_sw;
	uint64_t start_tick, curr_tick;
	char buf[12];
	struct timespec sp;
	int i2c_fp, quit;
	grounds_t data;
	config_t cfg;
	int addrs[] = {
		I2C_BUSADDR_HOTPLATE, I2C_BUSADDR_BOILER, I2C_BUSADDR_FILTER, 0x00
	};

	memset(&data, 0, sizeof(grounds_t));
	memset(buf, 0, 10);

	// read the config file first
	if (config_read(CONFIG_FILE, &cfg)) {
		ERR_AND_QUIT("error parsing config file");
	}

	i2c_fp = i2c_setup("/dev/i2c-2");
	if (i2c_fp < 0) {
		ERR_AND_QUIT("couldn't get an i2c file pointer");
	}

	boiler_sw = gpio_setup(GPIO_BOILER_SWITCH, INPUT);
	hotplate_sw = gpio_setup(GPIO_HOTPLATE_STATUS, INPUT);

	web_set_prefix(cfg.web_dir);
	web_daemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, cfg.web_port, NULL, NULL,
			&web_get, NULL, MHD_OPTION_END);
	if (web_daemon == NULL) {
		// error message here
		return 1;
	}

	// get the starting clock tick
	if (clock_gettime(CLOCK_MONOTONIC, &sp) < 0) {
		ERR_AND_QUIT("couldn't get current time");
	}
	start_tick = sp.tv_nsec * 1000;

	quit = 0;
	for (nonce = 0; !quit; nonce++) {
	// while (!quit) {
		/* do the loop (i2c reading) */
		coffee_doloop(i2c_fp, addrs, &data);

		data_dump(nonce, &data);

		/* now we need to read our switches */
		// if (gpio_input_read(boiler_sw) == HIGH) {
		// 	data.commands |= MSTR_BOILER_ON;
		// }

		// if (gpio_input_read(hotplate_sw) == HIGH) {
		data.commands |= MSTR_HOTPLATE_ON;
		// data.commands = 0;
		// }

		if (check_change(&data, &data)) {
			// csv_out(cfg.out_file, &data);
		}

		// screen dump
		printf("scale: %ld\thotplate temp: %lf\tboiler temp: %lf\t flow: %ld\n",
				data.slave.hotplate.mass,
				data.slave.hotplate.temp,
				data.slave.boiler.temp,
				data.slave.boiler.flow);

		// sleep for some time don't waste processor power
		if (clock_gettime(CLOCK_MONOTONIC, &sp) < 0) {
			ERR_AND_QUIT("couldn't get current time");
		}

		usleep(500 * 1000);

		// curr_tick = sp.tv_nsec * 1000000000 - start_tick;
		// if (curr_tick < UPDATES_PER_SEC) {
		// 	usleep(UPDATES_PER_SEC - curr_tick);
		// }
	}

	// clean up our GPIO pins
	MHD_stop_daemon(web_daemon);
	gpio_free(boiler_sw);
	gpio_free(hotplate_sw);

	return 0;
}

int check_change(grounds_t *curr, grounds_t *new)
{
	// compare the two structures to see if anything changed
	return 1;
}

void coffee_doloop(int file, int *addr, grounds_t *data)
{
	int i, val;

	/* read from every address in the NULL terminated buffer */
	for (i = 0; addr[i]; i++, val = 0) {
		switch (addr[i]) {
		case I2C_BUSADDR_HOTPLATE:
			val = i2c_read(file, addr[i], sizeof(hotplate_t),
					&(data->slave.hotplate));
			break;

		case I2C_BUSADDR_BOILER:
		 	// val = i2c_read(file, addr[i], sizeof(boiler_t),
			// 		&(data->slave.boiler));
			break;

		case I2C_BUSADDR_FILTER:
		// 	i2c_read(file, addr[i], sizeof(filter_t),
		//			&(data->slave.filter));
			break;

		default:
			fprintf(stderr, "invalid address in coffee_doloop\n");
			break;
		}

		if (val == -1) {
			fprintf(stderr, "dev : %d, err: %s\n", addr[i], strerror(errno));
		}
	}

	/* set values in data depending on what comes in */

	/* now, write the concatenation back to every device */
	for (i = 0; addr[i]; i++) {
		if (I2C_BUSADDR_HOTPLATE == addr[i]) {
			i2c_write(file, addr[i], sizeof(data->commands), &(data->commands));
		}
	}
}

void coffee_control(grounds_t *data)
{
}

/* GPIO HELPER FUNCTIONS LIVE BELOW (very libsoc dependent) */
gpio *gpio_setup(int pin_num, int direction)
{
	gpio *ptr;
	char buf[128];

	ptr = libsoc_gpio_request(pin_num, LS_GPIO_SHARED);

	if (ptr == NULL) {
		sprintf(buf, "error setting up pin %d\n", pin_num);
		ERR_AND_QUIT(buf);
		exit(1);
	}

	libsoc_gpio_set_direction(ptr, direction);

	if (libsoc_gpio_get_direction(ptr) != direction) {
		sprintf(buf, "error setting up direction for pin %d\n", pin_num);
		ERR_AND_QUIT(buf);
	}

	return ptr;
}

int gpio_free(gpio *ptr)
{
	if (ptr) {
		if (libsoc_gpio_free(ptr) == EXIT_FAILURE) {
			ERR_AND_QUIT("gpio free was unsuccessful");
		}
	}

	return 0;
}

int gpio_input_read(gpio *ptr)
{
	int dir;
	dir = libsoc_gpio_get_direction(ptr);

	if (dir == DIRECTION_ERROR) {
		ERR_AND_QUIT("couldn't get direction. oops");
	}

	return dir;
}

void data_dump(long nonce, struct grounds_t *ptr)
{
	printf("%ld\n", nonce);

	printf("\tcommand : 0x%02X\n", ptr->commands);

	printf("\thotplate: mass: %" PRIu64 "\ttemp: %f deg F\tflags: 0x%02X\n",
			ptr->slave.hotplate.mass,
			(ptr->slave.hotplate.temp / 128.0) * (9 / 5) + 32,
			ptr->slave.hotplate.flags);

	printf("\tboiler  : flow: %" PRIu64 "\ttemp: %f deg F\tflags: 0x%02X\n",
			ptr->slave.boiler.flow,
			(ptr->slave.boiler.temp / 128.0) * (9 / 5) + 32,
			ptr->slave.boiler.flags);
}

