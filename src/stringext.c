/*
 * Brian Chrzanowski
 * Sat Sep 08, 2018 06:10
 *
 * string helper functions
 *
 * examples include parsing regular expressions, tossing quotes on a string...
 *
 * Regular Expression Support
 *		the regular expression code can be found here:
 *		https://www.cs.princeton.edu/courses/archive/spr09/cos333/beautiful.html
 *	
 *		its rules are as follows:
 *			c - matches any literal character c
 *			. - matches any single character
 *			^ - matches the beginning of the input string
 *			$ - matches the end of the input string
 *			* - matches zero or more occurrences of the previous character
 */

#include "stringext.h"

/* match: search for regexp anywhere in text */
int regex_match(char *regexp, char *text)
{
	if (regexp[0] == '^')
		return regex_matchhere(regexp+1, text);

	do {    /* must look even if string is empty */
		if (regex_matchhere(regexp, text))
		return 1;
	} while (*text++ != '\0');

	return 0;
}

/* matchhere: search for regexp at beginning of text */
int regex_matchhere(char *regexp, char *text)
{
	if (regexp[0] == '\0')
		return 1;

	if (regexp[1] == '*')
		return regex_matchstar(regexp[0], regexp+2, text);

	if (regexp[0] == '$' && regexp[1] == '\0')
		return *text == '\0';

	if (*text!='\0' && (regexp[0]=='.' || regexp[0]==*text))
		return regex_matchhere(regexp+1, text+1);

	return 0;
}

/* matchstar: search for c*regexp at beginning of text */
int regex_matchstar(int c, char *regexp, char *text)
{
	do {    /* a * matches zero or more instances */
		if (regex_matchhere(regexp, text))
			return 1;
	} while (*text != '\0' && (*text++ == c || c == '.'));

	return 0;
}
