/*
 * Brian Chrzanowski
 * Sat Sep 15, 2018 19:01
 *
 * Web interface for Brewing Grounds
 * 
 * Uses libmicrohttpd to handle web integration and threading models
 */

#include <microhttpd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "io.h"
#include "stringext.h"

#define PAGE_ERROR "404.html"

// some helpers, to redir to index.html, when there isn't actually
// anything interesting going on in their request

int make_response(struct MHD_Connection *conn, char *file, int resp_code);
char *get_page(char *request);

static char *web_prefix;
//extern 
char *web_idx = "/index.html";

void web_set_prefix(char *prefix)
{
	web_prefix = prefix;
}

int web_get (void *cls, struct MHD_Connection *conn,
							const char *url,
							const char *method, const char *version,
							const char *upload_data,
							size_t *upload_size, void **ptr)
{
	char buf[512];
	char *file_name;
	int tmp, ret, code;

	if (strcmp(method, "GET") != 0) {
		return MHD_NO; // unexpected method
	}

	if (&tmp != *ptr) {
		// the first time only the headers are valid, don't respond
		*ptr = &tmp;
		return MHD_YES;
	}

	if (*upload_size != 0) {
		return MHD_NO; // uploading data in a GET method???
	}

	*ptr = NULL; // clear context pointer

	file_name = get_page((char *)url);

	// file_name = (char *)url;
	sprintf(buf, "%s%s", web_prefix, file_name);

	if (access(buf, F_OK) != -1) {
		code = MHD_HTTP_OK;
		ret = make_response(conn, buf, code);
	} else {
		printf("doesn't exist... using: ");
		sprintf(buf, "%s/%s", web_prefix, PAGE_ERROR);
		code = MHD_HTTP_NOT_FOUND;
		ret = make_response(conn, buf, code);

	}

	return ret;
}

int make_response(struct MHD_Connection *conn, char *file, int resp_code)
{
	int ret;
	struct MHD_Response *resp;
	char *buf;

	ret = MHD_NO;
	resp = NULL;
	buf = file_to_buffer(file);

	if (buf) {
		resp = MHD_create_response_from_buffer(
				strlen(buf), buf, MHD_RESPMEM_PERSISTENT);

		ret = MHD_queue_response(conn, resp_code, resp);

		MHD_destroy_response(resp);
	}

	return ret;
}

char *get_page(char *request)
{
	if (regex_match("^/$", request)) {
		return web_idx;
	} else {
		return request;
	}
}

