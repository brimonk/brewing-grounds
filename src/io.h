
#ifndef BG_IO
#define BG_IO

#include "brew/brewing-grounds.h"
#include "helper.h"

int i2c_setup(char *filename);
int i2c_write(int file, int addr, int n, void *ptr);
int i2c_read(int file, int addr, int n, void *ptr);

int config_read(char *filename, config_t *cfg);
void config_set(char *str, config_t *cfg);

int csv_headercheck(FILE *fp);
int csv_out(char *filename, grounds_t *data);

char *file_to_buffer(char *name);

#endif
