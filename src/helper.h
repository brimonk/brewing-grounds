/*
 * Brian Chrzanowski
 * Sat Sep 08, 2018 03:59
 *
 * Simply some helper macros and defs
 */

#ifndef BM_HELPER
#define BM_HELPER

#define BUFFER_SIZE 64

// we'll define the configuration structure here
typedef struct config_t {
	int web_port;
	int i2c_ups;
	char web_dir[BUFFER_SIZE];
	char out_file[BUFFER_SIZE];
} config_t;

void err_and_quit(char *msg, char *file, int line);
#define ERR_AND_QUIT(A) (err_and_quit((A), __FILE__, __LINE__))

#endif
