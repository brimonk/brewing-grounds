/*
 * Brian Chrzanowski
 * Sat Sep 08, 2018 06:12
 *
 * string extensions header file
 */

#ifndef BG_STRINGEXT
#define BG_STRINGEXT

int regex_match(char *regexp, char *text);
int regex_matchhere(char *regexp, char *text);
int regex_matchstar(int c, char *regexp, char *text);

#endif
