/*
 * web.c header
 */

int web_set_prefix(char *prefix);
int web_get (void *cls, struct MHD_Connection *conn,
							const char *url,
							const char *method, const char *version,
							const char *upload_data,
							size_t *upload_size, void **ptr);
