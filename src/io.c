/*
 * Brian Chrzanowski
 * Sat Sep 08, 2018 06:04
 *
 * Brewing Grounds Input Output Functions
 *		i2c
 *		config file
 *		output file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <errno.h>
#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "helper.h"
#include "stringext.h"
#include "io.h"
#include "brew/brewing-grounds.h"

// be careful with adding more columns to the spreadsheet (have correct macro)
// last one shouldn't have a comma
#define SPREADSHEET_REGEX \
	"\"unix_epoch\"," \
	"\"boiler_temp_f\"," \
	"\"boiler_heater_on\"," \
	"\"boiler_blanket_on\"," \
	"\"boiler_jug_pump_on\"," \
	"\"boiler_boiler_pump_on\""
#define SPREADSHEET_BOILER \
	"\"%ld\"," \
	"\"%f\"," \
	"\"%d\"," \
	"\"%d\"," \
	"\"%d\"," \
	"\"%d\""
#define SPREADSHEEET_FILTER\
	"\"%ld\"," \
	"\"%f\"," \
	"\"%d\"," \
#define SPREADSHEET_WARNING "WARNING : provided spreadsheet doesn't have"\
	" defined header.\ncontinue at own risk"

int i2c_setup(char *filename)
{
	int file;
	file = open(filename, O_RDWR);

	if (file < 0) {
		// should do some errno handling
		return -1;
	}

	return file;
}

int i2c_read(int file, int addr, int n, void *ptr)
{
	// read n bytes from the device with addr on the bus, using file as the
	// fp, storing the results in ptr.
	//
	// returns number of bytes read, or -1 if there was an error

	int val;

	// set the device we're reading, just to make sure
	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		// errno error handling here
		return -1;
	}

	// read some bytes
	val = read(file, ptr, n);

	if (val != n) {
		// errno error handling here
	}

	return val;
}


int i2c_write(int file, int addr, int n, void *ptr)
{
	// write n bytes from ptr to device with addr, using file as the fp
	// returns the number of bytes written, or -1 on error

	int val;

	// set the device we're writing to, just in case
	if (ioctl(file, I2C_SLAVE, addr) < 0) {
		// errno error handling here
		return -1;
	}

	val = write(file, ptr, n);

	if (val != n) {
		// errno error handling here
	}

	return val;
}

int config_read(char *filename, config_t *cfg)
{
	FILE *fp = fopen(filename, "r");
	char buf[512];

	if (fp) {
		// check if the line matches the regex for a config
		while (fgets(buf, sizeof(buf), fp) == buf) {
			if (regex_match("[.*]", buf)) { // if we have a square bracket pair
				config_set(buf, cfg);
			}
		}

		fclose(fp);
	} else {
		return 1;
	}

	return 0;
}

void config_set(char *str, config_t *cfg)
{
	char *bck;

	bck = strrchr(str, ']');

	str++;
	*bck = 0; // effectively null out the string

	// now we can parse for its value (up to the colon), with a big if chain :)
	if (strncmp(str, "OUTPUT_CSV", strlen("OUTPUT_CSV")) == 0) {
		str = strchr(str, ':');
		str++;
		strncpy(cfg->out_file, str, BUFFER_SIZE);
	}

	if (strncmp(str, "WEBSERVER_DIR", strlen("WEBSERVER_DIR")) == 0) {
		str = strchr(str, ':');
		str++;
		strncpy(cfg->web_dir, str, BUFFER_SIZE);
	}

	if (strncmp(str, "WEBSERVER_PORT", strlen("WEBSERVER_PORT")) == 0) {
		str = strchr(str, ':');
		str++;
		cfg->web_port = atoi(str);
	}

	if (strncmp(str, "UPDATES_PER_SEC", strlen("UPDATES_PER_SEC")) == 0) {
		str = strchr(str, ':');
		str++;
		cfg->i2c_ups = atoi(str);
	}
}

int csv_out(char *filename, struct grounds_t *data)
{
	FILE *fp;

	fp = fopen(filename, "wa");

	if (fp) {
		if (csv_headercheck(fp)) {
			fprintf(fp, SPREADSHEET_REGEX); // write header
			fprintf(fp, "\n"); // (my) regex doesn't like escape characters
		}

		fprintf(fp, "\n");

		fclose(fp);
	} else {
		return 1;
	}

	return 0;
}

void csv_writeline(FILE *fp, grounds_t *data)
{
	// boiler information
	// fprintf(fp, SPREADSHEET_LINE,
	// 		(long)time(NULL), // placeholder for unix epoch
	// 		data->boiler.temp / 100.0,
	// 		data->boiler.flags & BOILER_HEATER_BIG ? 1 : 0,
	// 		data->boiler.flags & BOILER_HEATER_SMALL ? 1 : 0,
	// 		data->boiler.flags & BOILER_JUG_PUMP_ON ? 1 : 0,
	// 		data->boiler.flags & BOILER_BOILER_PUMP_ON ? 1 : 0);
	// // filter information
	// fprintf(fp, SPREADSHEET_LINE,
	// 		(long)time(NULL), // placeholder for unix epoch
	// 		data->boiler.temp / 100.0,
	// 		data->boiler.flags & BOILER_HEATER_BIG ? 1 : 0,
	// 		data->boiler.flags & BOILER_HEATER_SMALL ? 1 : 0,
	// 		data->boiler.flags & BOILER_JUG_PUMP_ON ? 1 : 0,
	// 		data->boiler.flags & BOILER_BOILER_PUMP_ON ? 1 : 0);
}

int csv_headercheck(FILE *fp)
{
	int amt;

	rewind(fp);
	amt = fseek(fp, 0, SEEK_END);

	if (amt) {
		// we have bytes, already have a header
		return 0;
	}

	// don't have bytes, don't have a header
	return 1;
}

char *file_to_buffer(char *filename)
{
	/* slurp up filename's contents to memory */
	FILE *fileptr;
	char *buffer;
	uint64_t filelen;

	buffer = NULL;
	filelen = 0;

	fileptr = fopen(filename, "r");

	if (fileptr) {
		/* jump from start to finish of a file to get its size */
		fseek(fileptr, 0, SEEK_END);
		filelen = ftell(fileptr);

		/* go back to the start */
		rewind(fileptr);

		buffer = malloc(filelen + 1); /* room for the file and a NULL byte */

		if (buffer) {
			memset(buffer, 0, filelen + 1);
			fread(buffer, filelen, 1, fileptr);
		}

		fclose(fileptr);
	}

	return buffer; /* caller frees the memory if non-NULL */
}

