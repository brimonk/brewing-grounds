#include <stdio.h>
#include <stdlib.h>

#include "helper.h"

void err_and_quit(char *msg, char *file, int line)
{
	fprintf(stderr, "%s:%d %s\n", file, line, msg);
	exit(1);
}
