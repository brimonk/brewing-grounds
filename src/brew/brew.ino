/*
 * Brian Chrzanowski
 * Jake Harris
 * Wed Aug 15, 2018 21:57
 *
 * the brewing "main"
 *
 * Important Notes:
 *
 *		No module can use pins 4 or 6 for anything interesting beacuse the
 *		ATTiny Serial interfaces (SDA, SCK) are on those pins.
 *
 *		We have a convention of turning things off that make things move. IE,
 *		pumps, motors, hotplates, boilers, etc. Convention says that only the
 *		i2c master can turn on devices that make things move; however, once it
 *		turns them on, both the master and the slave can turn them off. Slave
 *		for safety purposes, master for application need.
 */

#include "brewing-grounds.h"

#define PIN_SET(a, b) (digitalWrite((a), (b)))
#define I2C_RECV_BUFSIZE 16

// uncomment the #define for individual devices
#define CURRENT_DEVICE I2C_BUSADDR_BOILER
// #define CURRENT_DEVICE I2C_BUSADDR_HOTPLATE
// #define CURRENT_DEVICE I2C_BUSADDR_FILTER

uint8_t heater_check(double curr, double *range);
// store a buffer of bytes (16) and each module parses it when they
// get a chance. 16 bytes for future software expansion
uint8_t cmd;

void setup()
{
	// defer to each module's specific setup
	module_setup();
}

void loop()
{
	// defer to each module's specific loop
	module_loop();
}
