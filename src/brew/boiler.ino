/*
 * Brian Chrzanowski
 * Jake Harris
 * Wed Aug 22, 2018 21:26
 *
 * The Boiler Module
 *
 *		Controls
 *			Thermometer
 *				Data
 *			Jug Pump
 *				Toggle
 *				Hall Effect Sensor
 *			Boiler Pump
 *				Toggle
 *			Heating
 *				High Heat Toggle
 *				Low Heat Toggle
 *			Depth Probe
 *				Power
 *				Low Signal
 *				Empty Signal
 *			Water Flow Meter
 *				Data
 *			I2C
 *				Clock
 *				Data
 *
 * Notes:
 *
 *		Instead of using timers on the ATTiny or at all on the Arduino
 *		platform, we do something particularly disgusting. We simply store a
 *		set of TIME_DELTA_STORAGE points, and after we've achieved
 *		TIME_DELTA_STORAGE, we simply average the time those interrupts took
 *		place. On the TIME_DELTA_STORAGEth interrupt, we average all of the
 *		points, and if the average is higher than we've previously seen, we're
 *		sucking air, when we were sucking water. If the opposite is true, the
 *		pump is sucking water.
 *
 *		This method (hopefully) is portable across various pumps, as long as the
 *		assumption their RPM will fluctuate between iterations.
 */

#if CURRENT_DEVICE == I2C_BUSADDR_BOILER

#include <stdio.h>
#include <stdint.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include "brewing-grounds.h"

#define CONV_CELCIUS 0
#define CONV_FAHRENHEIT 1

// pin definitions
#define PIN_HIGHTEMP 0
#define PIN_LOWTEMP 1
#define PIN_DEPTHSENS 2
#define PIN_SDA 4
#define PIN_SCK 6
#define PIN_THERMOMETER 7
#define PIN_JUGPUMP 9
#define PIN_BOILERPUMP 10

// define some other storage
boiler_t boiler_data;

OneWire oneWire(PIN_THERMOMETER);
DallasTemperature dallastemp(&oneWire);

DeviceAddress ambient = {0x28, 0xFF, 0x89, 0x20, 0xB2, 0x17, 0x05, 0x4E};
int depth;

double temp_ranges[] = {
	DEVICE_DISCONNECTED_F,
	180, // low temp
	202 // high temp
};

void module_setup()
{
	// every module needs to setup its own I2C reading
	// Wire.begin(I2C_BUSADDR_BOILER);
	// Wire.onRequest(module_i2c_request);
	// Wire.onReceive(module_i2c_recieve);

	// we have to load up the dallas temperature library to read from the
	// sensor
	dallastemp.begin();
	dallastemp.setWaitForConversion(false);
	dallastemp.setResolution(12); // 12 bits o precision

	// set inputs and outputs
	pinMode(PIN_JUGPUMP, OUTPUT);
	pinMode(PIN_BOILERPUMP, OUTPUT);
	pinMode(PIN_HIGHTEMP, OUTPUT);
	pinMode(PIN_LOWTEMP, OUTPUT);

	pinMode(PIN_DEPTHSENS, INPUT_PULLUP);
	pinMode(PIN_SCK, INPUT);
	// we don't touch the thermometer pin. the library does that :)
	depth = 0;
}

void module_loop()
{
	float tmp_temp;
	int depth;
	uint8_t tmp;
	uint8_t i;

	// Temperature Control
	dallastemp.requestTemperatures();
	boiler_data.temp = dallastemp.getTemp(ambient);

	tmp_temp = temp_int_to_float(boiler_data.temp);

	// check the three states we can be in
	i = heater_check(tmp_temp, &temp_ranges[0]);

	// depth sense
	depth = analogRead(PIN_DEPTHSENS);

	if (depth > 1000) {
		cmd |= MSTR_BOILERFULL;
	} else {
		cmd &= ~MSTR_BOILERFULL;
	}

	if (digitalRead(PIN_SCK) == HIGH) {
		if (cmd & MSTR_BOILERFULL) {
			if (boiler_control(i)) { // if we have a good temperature
				digitalWrite(PIN_BOILERPUMP, HIGH);
				digitalWrite(PIN_JUGPUMP, LOW);
			} else {
			}
		} else {
			digitalWrite(PIN_BOILERPUMP, LOW);
			digitalWrite(PIN_JUGPUMP, HIGH);
		}

	} else {
		// cleanup - drive everything low
		digitalWrite(PIN_BOILERPUMP, LOW);
		digitalWrite(PIN_JUGPUMP, LOW);
		digitalWrite(PIN_HIGHTEMP, LOW);
		digitalWrite(PIN_LOWTEMP, LOW);
	}

	// store the current status of things in the structures, to be sent
	// to the master
}

int boiler_control(int cond)
{
	// returns 1 - we can pump out, 0 - we can't pump out
	int high, low, val;

	switch (cond) {
	case BREWG_TEMP_LOW:
		// we're too low
		high = HIGH;
		low = LOW;
		val = 0;
		break;

	case BREWG_TEMP_HIGH:
		high = LOW;
		low = LOW;
		val = 1;
		break;

	case BREWG_TEMP_JUSTRIGHT:
		high = LOW;
		low = HIGH;
		val = 1;
		break;

	case BREWG_ERROR_CODE:
		// error code here
		break;
	}

	digitalWrite(PIN_HIGHTEMP, high);
	digitalWrite(PIN_LOWTEMP, low);

	return val;
}

void module_i2c_request()
{
	// write all of the data we require over i2c
	// this structure is defined in the BrewingGrounds library
	Wire.write((char *)&boiler_data, sizeof(boiler_t));
}

void module_i2c_recieve(int bytes)
{
	if (Wire.available()) {
		// read the 1 command byte
		cmd = Wire.read();
	}
}

#endif
