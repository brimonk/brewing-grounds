/*
 * Brian Chrzanowski
 * Jake Harris
 *
 * Fri Aug 24, 2018 00:03
 *
 * Hotplate Module
 *
 *		Controls
 *			Scale
 *				Clock
 *				Data
 *			Thermometer
 *				Data
 *			Hotplate
 *				Relay (toggle)
 *			I2C
 *				Data
 *				Clock
 */

#if CURRENT_DEVICE == I2C_BUSADDR_HOTPLATE

#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <Q2HX711.h>
#include "brewing-grounds.h"

#define PIN_SCALE_CLK A2
#define PIN_SCALE_DATA A3
#define PIN_SDA 4
#define PIN_SCK 6
#define PIN_HOTPLATE_RELAY 5
#define PIN_DEBUG_LED 7
#define PIN_THERMOMETER 9

hotplate_t hotplate_data;

DeviceAddress ambient = {0x28, 0xFF, 0x8F, 0xC4, 0xB2, 0x17, 0x04, 0xCB};
OneWire oneWire(PIN_THERMOMETER);
DallasTemperature dallastemp(&oneWire);

Q2HX711 hx711(PIN_SCALE_CLK, PIN_SCALE_DATA);

// the hotplate honestly needs a single byte to determine what it needs
// to do
unsigned long start_tick, curr_tick;

double temp_ranges[] = {
	DEVICE_DISCONNECTED_F,
	99.9, // low temp
	100.1 // high temp
};

#define WEIGHT_HOTPLATE 1.6
#define WEIGHT_EMPTY_POT 1.4
#define WEIGHT_CUPOFJOE 1.0

void module_setup()
{
	Wire.begin(I2C_BUSADDR_HOTPLATE);
	Wire.onRequest(module_i2c_request);
	Wire.onReceive(module_i2c_receive);

	// we have to load up the dallas temperature library to read the sensor
	dallastemp.begin();
	dallastemp.setWaitForConversion(false);
	dallastemp.setResolution(12); // 12 bits o precision

	// hotplate is the only "normal" pin
	pinMode(PIN_HOTPLATE_RELAY, OUTPUT);
	pinMode(PIN_DEBUG_LED, OUTPUT);

	pinMode(PIN_SDA, INPUT);

	start_tick = millis();
	curr_tick = 0;

	cmd |= MSTR_HOTPLATE_ON;
}

// this function should only ever be called in I2C_BUSADDR_HOTPLATE
void module_loop()
{
	float tmp_temperature;
	uint8_t i;
	uint8_t tmp;

	// Temperature Control
	dallastemp.requestTemperatures();
	hotplate_data.temp = dallastemp.getTemp(ambient);

	tmp_temperature = temp_int_to_float(hotplate_data.temp);

	// check the three states we can be in
	i = heater_check(tmp_temperature, &temp_ranges[0]);

	// scale
	hotplate_data.mass = hx711.read();

	if (digitalRead(PIN_SDA) == HIGH /*&& cmd & MSTR_HOTPLATE_ON*/) {
		hotplate_control(i); // pin controlled in function
	} else {
		digitalWrite(PIN_HOTPLATE_RELAY, LOW);
		hotplate_data.flags &= ~HOTPLATE_HOTPLATE_ON;
	}

	// debug LED blinking (just to make sure it works)
	curr_tick = millis() - start_tick;
	if (curr_tick < 1000) {
		start_tick = millis();
		digitalWrite(PIN_DEBUG_LED, !digitalRead(PIN_DEBUG_LED));
	}

}

void hotplate_control(int cond)
{
	/*
	 * control the hotplate, based on temperature-range code provided
	 * also set hotplate data flag for the hotplate status
	 */

	int setting;

	switch (cond) {
	case BREWG_TEMP_LOW:
	case BREWG_TEMP_JUSTRIGHT:
		// we're too low (or keeping the plate on is okay)
		setting = HIGH;
		hotplate_data.flags |= HOTPLATE_HOTPLATE_ON;
		break;

		// do nothing
		break;

	case BREWG_TEMP_HIGH:
		setting = LOW;
		hotplate_data.flags &= ~HOTPLATE_HOTPLATE_ON;
		break;

	case BREWG_ERROR_CODE:
	default:
		// error code here
		// blink_led_err(PIN_DEBUG, 3, 300);
		return;
	}

	// finally set the pin
	PIN_SET(PIN_HOTPLATE_RELAY, setting);
}

void module_i2c_request()
{
	Wire.write((char *)&hotplate_data, sizeof(hotplate_t));
}

void module_i2c_receive(int bytes)
{
	if (Wire.available()) {
		// read the 1 byte for commands
		cmd = Wire.read();
	}
}

#endif
