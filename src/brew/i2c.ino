/*
 * Brian Chrzanowski
 * Jake Harris
 * Wed Aug 22, 2018 20:36
 *
 * Our i2c library, with arbitrary data lengths
 */

#include <Wire.h>
#include "./brewing-grounds.h"

uint8_t i2c_read(uint8_t addr, char *buf, uint8_t buflen)
{
	int bytes;
	Wire.requestFrom(addr, buflen); // sizeof(struct hotplate_t));

	for (bytes = 0; Wire.available() && bytes < buflen; bytes++) {
		buf[bytes] = Wire.read();
	}

	return bytes;
}

uint8_t i2c_write(uint8_t addr, char *buf, uint8_t buflen)
{
	int bytes;

	Wire.beginTransmission(addr);

	for (int i = 0; i < buflen; i++) {
		Wire.write(buf[i]);
	}

	Wire.endTransmission();

	return bytes;
}

