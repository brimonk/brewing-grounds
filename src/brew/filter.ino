/*
 * Brian Chrzanowski
 * Jake Harris
 * Fri Aug 24, 2018 00:17
 *
 * Filter Module
 *
 *		Controls
 *			Grounds Wheel
 *				Hall Effect Sensor
 *					Motor
 *					Wheel
 *				Relay
 *			Filter Wheel
 *				Hall Effect Sensor
 *					Motor
 *					Wheel
 *				Relay
 *			I2C
 *				Clock
 *				Data
 */

#if CURRENT_DEVICE == I2C_BUSADDR_FILTER

#include <Wire.h>
#include "brewing-grounds.h"

// define some pin values
#define PIN_HALLEFF_GROUNDS_WHEEL 0
#define PIN_HALLEFF_GROUNDS_MOTOR 1
#define PIN_HALLEFF_ROLL_MOTOR 2
#define PIN_HALLEFF_ROLL_WHEEL 3
#define PIN_RELAY_WHEEL 5
#define PIN_RELAY_FILTER 7

// define other miscellaneous constants

filter_t filter_data;

void module_setup()
{
	// every module needs to setup its own I2C reading
	Wire.begin(I2C_BUSADDR_FILTER);
	Wire.onRequest(module_i2c_request);
	Wire.onReceive(module_i2c_receive);

	// we have two polling hall effect sensors
	pinMode(PIN_HALLEFF_GROUNDS_WHEEL, INPUT);
	pinMode(PIN_HALLEFF_ROLL_WHEEL, INPUT);

	// pins to toggle the relays
	pinMode(PIN_RELAY_WHEEL, OUTPUT);
	pinMode(PIN_RELAY_FILTER, OUTPUT);

	// set up interrupts for the motor hall effect sensors
	attachInterrupt(digitalPinToInterrupt(PIN_HALLEFF_GROUNDS_MOTOR),
			isr_motor_grounds, FALLING);
	attachInterrupt(digitalPinToInterrupt(PIN_HALLEFF_ROLL_MOTOR),
			isr_motor_wheel, FALLING);
}

void module_loop()
{
}

void module_i2c_request()
{
	Wire.write((char *)&filter_data, sizeof(filter_t));
}

void module_i2c_receive(int bytes)
{
	char *ptr;

	ptr = (char *)&datum;

	for (int i = 0; i < bytes; i++) {
		*ptr++ = Wire.read();
	}
}

// isrs for our motor relays
void isr_motor_grounds()
{
}

void isr_motor_wheel()
{
}

#endif
