/*
 * Brian Chrzanowski
 * Thu Aug 23, 2018 22:16
 *
 * brewing-grounds library header file
 *
 * Caveats
 *
 *		All floating point numbers, to ensure they get from slave devices
 *		to the master CHIP, are assumed to be transformed into a 32bit
 *		integer before transfer
 */

#ifndef BREWING_GROUNDS_H
#define BREWING_GROUNDS_H

#include <stdint.h>

/*
 * first, define some common device IDs
 *
 * These can be any number, such that 8 <= x <= 127, as defined by I2C
 */

#define I2C_BUSADDR_MASTER 0x08
#define I2C_BUSADDR_SLAVE 0x09
#define I2C_BUSADDR_HOTPLATE 0x0a
#define I2C_BUSADDR_BOILER 0x0b
#define I2C_BUSADDR_FILTER 0x0c

/* simply some common codes to make random integers more meaningful */

#define BREWG_TEMP_LOW 0
#define BREWG_TEMP_HIGH 1
#define BREWG_TEMP_ERR 2
#define BREWG_TEMP_JUSTRIGHT 3

#define BREWG_ERROR_CODE 255

/* define data structures for each module */

// hotplate bit flags
#define HOTPLATE_HOTPLATE_ON 0x01
#define HOTPLATE_HOTPLATE_POT_LOW 0x02
#define HOTPLATE_HOTPLATE_POT_OFF 0x04
typedef struct hotplate_t {
	uint64_t mass;
	uint16_t temp;
	uint8_t flags;
} hotplate_t;

// filter bit flags
#define FILTER_FILTER_MOTOR_ON 0x01
#define FILTER_FILTER_WHEEL_ON 0x02
#define FILTER_GROUNDS_MOTOR_ON 0x04
#define FILTER_GROUNDS_WHEEL_ON 0x08
typedef struct filter_t {
	uint32_t water_flow;
	uint8_t flags;
} filter_t;

// boiler bit flags
#define BOILER_BOILER_PUMP_ON 0x01
#define BOILER_BOILER_WATER_LOW 0x02
#define BOILER_BOILER_WATER_EMPTY 0x04
#define BOILER_JUG_PUMP_ON 0x08
#define BOILER_JUG_EMPTY 0x10
#define BOILER_FILTER_TRANSFER_ON 0x20
#define BOILER_HEATER_BIG 0X40
#define BOILER_HEATER_SMALL 0X80
typedef struct boiler_t {
	uint64_t flow;
	uint16_t temp;
	uint8_t flags;
} boiler_t;

typedef struct slave_t {
	boiler_t boiler;
	filter_t filter;
	hotplate_t hotplate;
} slave_t;

#define MSTR_BOILER_ON 0x01
#define MSTR_HOTPLATE_ON 0x02
#define MSTR_JUGPUMP_ON 0x04
#define MSTR_BOILERPUMP_ON 0x08
#define MSTR_BOILERFULL 0x10
typedef struct grounds_t {
	slave_t slave;
	uint8_t commands;
} grounds_t;

#endif
