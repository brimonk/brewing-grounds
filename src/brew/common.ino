#include "brewing-grounds.h"

uint8_t heater_check(double curr, double *range)
{
	/* current temperature in F */
	if (curr == range[BREWG_TEMP_ERR]) {
		return BREWG_ERROR_CODE;
	}

	if (range[BREWG_TEMP_HIGH] < curr) {
		return BREWG_TEMP_HIGH;
	}

	if (range[BREWG_TEMP_LOW] <= curr && curr <= range[BREWG_TEMP_HIGH]) {
		return BREWG_TEMP_JUSTRIGHT;
	}

	if (curr < range[BREWG_TEMP_LOW]) {
		return BREWG_TEMP_LOW;
	}
}

float temp_int_to_float(uint16_t value)
{
	return (value / 128) * (9/5) + 32;
}

