# Brian Chrzanowski
# Sat Sep 08, 2018 01:14
#
# Makefile for the Pocket CHIP's controlling code
CC = cc
LINKER = -ldl -lpthread -lsoc -lmicrohttpd
FLAGS = -Wall -g3 -O
TARGET = brewmaster
SOURCES = $(wildcard src/*.c)
OBJECTS = $(SOURCES:.c=.o)

all: $(TARGET)

%.o: %.c
	$(CC) -c $(FLAGS) -o $@ $<

clean: clean-obj clean-bin

clean-obj:
	rm -f $(OBJECTS)
	
clean-bin:
	rm -f $(TARGET)
	
$(TARGET): $(OBJECTS)
	$(CC) $(FLAGS) -o $(TARGET) $(OBJECTS) $(LINKER)

